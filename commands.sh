#!/bin/bash

# Start nodemon in the Backend directory in the background
(cd Backend && nodemon server.js) &

# Sleep for a moment to allow the server to start
sleep 2

# Start BrowserSync in the Frontend directory
(cd Frontend && browser-sync start --server --files "index.html, styles.css, script.js")
